# Автор говнокода: nekto@0city.ru

# требуется установка requests // pip install requests

import glob
import json
import re
import requests
import sys
from datetime import datetime
from os import path, makedirs

ROOT_DIR = path.join('.', '0chan')
BOARDS_FILENAME = path.join(ROOT_DIR, "boards.json")
API_URL = "https://0chan.hk/api/"

# сессия не обязательна, но в будущем может пригодиться
session = ""

log_filename = path.join(ROOT_DIR, "get0chan #%s.log" % datetime.now().strftime("%d.%m.%y -- %H-%M-%S"))
log_directory = path.dirname(log_filename)

if not path.exists(log_directory):
    makedirs(log_directory)

flog = open(log_filename, 'w+', encoding='utf-8')


def log_write(text, end="\n"):
    print(text, end=end)
    print(text, end=end, file=flog)


def file_write(filename, data, mode="text"):
    # говно тупое само не умеет создавать пути
    if not path.exists(path.dirname(filename)):
        makedirs(path.dirname(filename))
    if mode == "text":
        f = open(filename, 'tw', encoding='utf-8')  # encoding иначе сука пытается сохранить в 1251 и сосёт
    else:
        f = open(filename, "bw")
    f.write(data)
    f.close()


def get_thread(board, tid, ready=False):
    thread_filename = path.join(ROOT_DIR, "boards", board, tid + '.json')
    if path.isfile(thread_filename):
        if ready:
            log_write("Тема %s скачена, файл: %s" % (tid, thread_filename))
        else:
            log_write("Тема %s уже скачена, удалите файл %s чтоб перекачать" % (tid, thread_filename))
        thread_data = json.load(open(thread_filename, 'r', encoding='utf-8'))

        log_write("Данные темы " + tid + ":")
        log_write("Поиск изображений...")
        for threads in thread_data['posts']:
            for attachments in threads['attachments']:
                url = "https:" + attachments['images']['original']['url']
                pattern = re.compile("//(.*?0chan\.hk.*?)\?hash=.*?&exp=\d{10}")
                result = re.search(pattern, url)
                if result:
                    filepath = result.group(1)
                    image_fname = path.join(ROOT_DIR, "images", filepath)
                    if path.isfile(image_fname):
                        log_write("Изображение %s уже скачено, путь: %s" % (url, image_fname))
                    else:
                        log_write("Скачивание: " + url, "")
                        request = requests.get(url, stream=True)
                        file_write(image_fname, request.raw.read(), "binary")
                        log_write(" закончилось.")
                else:
                    log_write("Ошибка в разборе ссылки: " + url)

    else:
        log_write("Скачиваем тему %s, доски %s..." % (tid, board))
        link = API_URL + 'thread?thread=' + tid + '&session=' + session
        log_write("Ссылка: " + link)
        request = requests.get(link)
        file_write(thread_filename, request.text)
        log_write("Тема " + tid + " скачена")
        get_thread(board, tid, True)


def get_threads_list(board, pagenum, ready=False):
    global cursor
    board_threads_fname = path.join(ROOT_DIR, "boards", board, 'threads-page%s.json' % pagenum)
    if path.isfile(board_threads_fname):
        if ready:
            log_write("Обработка...")
        else:
            log_write("Список тем доски %s страница %s уже скачен, обработка..." % (board, pagenum))
        threads_data = json.load(open(board_threads_fname, 'tr', encoding='utf-8'))
        # а вдруг ошибка (а они бывают, учитывая что доска может быть уже удалена вместе с темами)
        if 'threads' in threads_data.keys():
            cursor = threads_data['pagination']['cursor']
            if pagenum > 1:
                log_write("Текущий курсор: " + cursor)
            else:
                log_write("Получен курсор: " + cursor)

            if len(threads_data['threads']):
                for threads in threads_data['threads']:
                    get_thread(board, threads['thread']['id'])

                if threads_data['pagination']['hasMore']:
                    log_write("Есть ещё страницы.")
                    get_threads_list(board, pagenum + 1)
                else:
                    log_write("Нету больше страниц.")
            else:
                log_write("В доске нет тем.")

        else:
            log_write("Отсутствует ключ threads, данные:")
            log_write(str(threads_data))
            cursor = ""
            log_write("Курсор сброшен.")
    else:
        log_write("Скачиваем темы доски " + board + " страница " + str(pagenum) + "...")
        if pagenum > 1:
            page = "&page=" + str(pagenum)
            if cursor == "":
                log_write("Ошибка, курсор не установлен для последующих страниц! Аварийный выход.")
                log_write("Файл: " + board_threads_fname)
                sys.exit(1)
            cursor = '&cursor=' + cursor
        else:
            page = ""
            cursor = ""
        link = API_URL + 'board?dir=' + board + page + cursor + '&session=' + session
        log_write("Скачивание: " + link, "")
        request = requests.get(link)
        file_write(board_threads_fname, request.text)
        log_write(" закончилось. ", "")
        get_threads_list(board, pagenum, True)


def get_boards(ready=False):
    # если файл борд уже скачен, сразу его обрабатываем без запроса
    if path.isfile(BOARDS_FILENAME):
        if ready:
            log_write("Обработка...")
        else:
            log_write("Файл борд уже скачен, обработка...")
        # TODO: дальше перекачивать каждый раз файд досок чтоб выкачивать новые параши
        boards_data = json.load(open(BOARDS_FILENAME, 'tr', encoding='utf-8'))

        for board in boards_data['boards']:
            log_write("Обходим доски...")
            # TODO: перекачать всегда последний файл даже если там hasMore: false
            # чтоб смочь качать дальше без перекачки всего, но когда cursor протухает?
            # не смотрим на порядок выдачи ибо он может отличаться в зависимости от ОС, нам нужно только число файлов
            board_dir = path.join(ROOT_DIR, 'boards', board['dir'])
            board_threads_glob = path.join(board_dir, 'threads*.json')
            board_threads_count = len(glob.glob(board_threads_glob))
            if not ready:
                log_write("Уже скаченное число страниц доски %s: %s" % (board['dir'], board_threads_count))

            if board_threads_count < 1:
                board_threads_count = 1  # скачаем, хуле
                log_write("Скаченных страниц не найдено. Скачиваем...")
            log_write("Текущая: %s" % board_threads_count)
            board_threads_filename = path.join(board_dir, 'threads-page%s.json' % board_threads_count)
            log_write("Путь: " + board_threads_filename)
            get_threads_list(board['dir'], board_threads_count)
    else:
        # если файл борд не скачен
        log_write("Скачиваем файл досок...")
        link = API_URL + 'board/list?session=' + session
        log_write("Ссылка: " + link)
        request = requests.get(link)
        file_write(BOARDS_FILENAME, request.text)
        get_boards(True)


get_boards()

# getThread("b", "1904")

log_write("Закончено.")

flog.close()
